package org.netbeans.modules.android.project.layout;

import com.android.ide.common.rendering.api.RenderSession;
import com.android.ide.common.rendering.api.Result;
import com.android.ide.common.resources.configuration.DensityQualifier;
import com.android.ide.common.resources.configuration.FolderConfiguration;
import com.android.ide.common.resources.configuration.KeyboardStateQualifier;
import com.android.ide.common.resources.configuration.NavigationMethodQualifier;
import com.android.ide.common.resources.configuration.NavigationStateQualifier;
import com.android.ide.common.resources.configuration.ScreenDimensionQualifier;
import com.android.ide.common.resources.configuration.ScreenHeightQualifier;
import com.android.ide.common.resources.configuration.ScreenOrientationQualifier;
import com.android.ide.common.resources.configuration.ScreenRatioQualifier;
import com.android.ide.common.resources.configuration.ScreenSizeQualifier;
import com.android.ide.common.resources.configuration.ScreenWidthQualifier;
import com.android.ide.common.resources.configuration.SmallestScreenWidthQualifier;
import com.android.ide.common.resources.configuration.TextInputMethodQualifier;
import com.android.ide.common.resources.configuration.TouchScreenQualifier;
import com.android.ide.common.resources.configuration.VersionQualifier;
import com.android.resources.Density;
import com.android.resources.Keyboard;
import com.android.resources.KeyboardState;
import com.android.resources.Navigation;
import com.android.resources.NavigationState;
import com.android.resources.ScreenOrientation;
import com.android.resources.ScreenRatio;
import com.android.resources.ScreenSize;
import com.android.resources.TouchScreen;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidGeneralData;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.netbeans.modules.android.project.api.AndroidClassPath;
import org.netbeans.modules.parsing.impl.indexing.CacheFolder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

/**
 *
 * @author radim
 */
public class RenderingServiceTest {
  private static final Logger LOG = Logger.getLogger(RenderingServiceTest.class.getName());
  
  private static final String ANT_DIR = System.getProperty("test.all.ant.home");
  private static AndroidTestFixture fixture;

  private static FileObject snakePrjDir;
  private static AndroidProject snakePrj;
  private static FileObject someLayout1;
  private static DalvikPlatform testedPlatform;

  @BeforeClass
  public static void setUpClass() throws Exception {
    File cacheFolder = File.createTempFile("cache", "");
    CacheFolder.setCacheFolder(FileUtil.toFileObject(cacheFolder));
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake");
    
    snakePrjDir = fixture.getProjectFolder("Snake");
    snakePrj = (AndroidProject) fixture.getProject("Snake");
    someLayout1 = snakePrjDir.getFileObject("res/layout/snake_layout.xml");

    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget("android-8");
  }

  /**
   * Creates a config. This must be a valid config like a device would return. This is to prevent issues where some
   * resources don't exist in all cases and not in the default (for instance only available in hdpi and mdpi but not in
   * default).
   *
   * @param size1
   * @param size2
   * @param screenSize
   * @param screenRatio
   * @param orientation
   * @param density
   * @param touchScreen
   * @param keyboardState
   * @param keyboard
   * @param navigationState
   * @param navigation
   * @param apiLevel
   * @return
   */
  public static FolderConfiguration createConfig(
      int size1,
      int size2,
      ScreenSize screenSize,
      ScreenRatio screenRatio,
      ScreenOrientation orientation,
      Density density,
      TouchScreen touchScreen,
      KeyboardState keyboardState,
      Keyboard keyboard,
      NavigationState navigationState,
      Navigation navigation,
      int apiLevel) {
    FolderConfiguration config = new FolderConfiguration();

    int width = size1, height = size2;
    switch (orientation) {
      case LANDSCAPE:
        width = size1 < size2 ? size2 : size1;
        height = size1 < size2 ? size1 : size2;
        break;
      case PORTRAIT:
        width = size1 < size2 ? size1 : size2;
        height = size1 < size2 ? size2 : size1;
        break;
      case SQUARE:
        width = height = size1;
        break;
    }

    int wdp = (width * Density.DEFAULT_DENSITY) / density.getDpiValue();
    int hdp = (height * Density.DEFAULT_DENSITY) / density.getDpiValue();

    config.addQualifier(new SmallestScreenWidthQualifier(wdp < hdp ? wdp : hdp));
    config.addQualifier(new ScreenWidthQualifier(wdp));
    config.addQualifier(new ScreenHeightQualifier(hdp));

    config.addQualifier(new ScreenSizeQualifier(screenSize));
    config.addQualifier(new ScreenRatioQualifier(screenRatio));
    config.addQualifier(new ScreenOrientationQualifier(orientation));
    config.addQualifier(new DensityQualifier(density));
    config.addQualifier(new TouchScreenQualifier(touchScreen));
    config.addQualifier(new KeyboardStateQualifier(keyboardState));
    config.addQualifier(new TextInputMethodQualifier(keyboard));
    config.addQualifier(new NavigationStateQualifier(navigationState));
    config.addQualifier(new NavigationMethodQualifier(navigation));
    config.addQualifier(width > height ? new ScreenDimensionQualifier(width, height)
        : new ScreenDimensionQualifier(height, width));
    config.addQualifier(new VersionQualifier(apiLevel));

    config.updateScreenWidthAndHeight();

    return config;
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  private FolderConfiguration createTestConfiguration(DalvikPlatform platform) throws Exception {
    FolderConfiguration folderCfg = createConfig(
        1280, 800, // size 1 and 2. order doesn't matter.
        // Orientation will drive which is w and h
        ScreenSize.XLARGE,
        ScreenRatio.LONG,
        ScreenOrientation.LANDSCAPE,
        Density.MEDIUM,
        TouchScreen.FINGER,
        KeyboardState.SOFT,
        Keyboard.QWERTY,
        NavigationState.EXPOSED,
        Navigation.NONAV,
        platform.getAndroidTarget().getVersion().getApiLevel());
    folderCfg.setVersionQualifier(new VersionQualifier(platform.getAndroidTarget().getVersion().getApiLevel()));
    return folderCfg;
  }

  private void buildProject(FileObject prjDir) throws Exception {
    File mvn = new File(ANT_DIR, "bin/ant"); // TODO add .exe on Windows
    // System.err.println("tmp " + tempFolder.getAbsolutePath());
    ProcessBuilder pb = new ProcessBuilder(Lists.newArrayList(
        mvn.getAbsolutePath(),
        "debug")).
        directory(FileUtil.toFile(prjDir)).redirectErrorStream(true);
    pb.environment().put("JAVA_HOME", System.getProperty("java.home"));
    final Process p = pb.start();
    Executors.newCachedThreadPool().submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        ByteStreams.copy(p.getInputStream(), System.out);
        return null;
      }
    }).get(60, TimeUnit.SECONDS);
    p.waitFor();
    prjDir.refresh();
    assertEquals(0, p.exitValue());
  }
  
  BufferedImage renderImage(RenderingService service, AndroidClassPath cpProvider, 
      Reader layoutXmlReader, String themeName, boolean isPrjTheme, String layoutName) throws RenderingException {
    RenderSession session = null;
    try {
      session = service.createRenderSession(null, cpProvider, layoutXmlReader, themeName, isPrjTheme, layoutName);
    } catch (Exception e) {
      throw new RenderingException(e);
    }
    if (session == null) {
      return null;
    }

    final Result result = session.getResult();
    if (!result.isSuccess()) {
      LOG.log(Level.FINE, "rendering failed: {0}", result.getErrorMessage());
      final Throwable exception = result.getException();
      if (exception != null) {
        throw new RenderingException(exception);
      }
      final String message = result.getErrorMessage();
      if (message != null) {
        LOG.info(message);
        throw new RenderingException();
      }
      return null;
    }

    return session.getImage();
  }

  /* This needs to load object instance from project. */
  @Test
  public void renderImageCustomView() throws Exception {
    // do a build first to create class files
    final AndroidProject proj = (AndroidProject) ProjectManager.getDefault().findProject(snakePrjDir);
    AndroidGeneralData data = AndroidGeneralData.fromProject(proj);
    data.setPlatform(DalvikPlatformManager.getDefault().findPlatformForTarget("android-8"));
    proj.update(data);
  
    buildProject(snakePrjDir);
    FileObject viewClzFo = snakePrjDir.getFileObject("bin/classes/com/example/android/snake/SnakeView.class");
    assertNotNull(viewClzFo);
    
    AndroidProject ap = (AndroidProject) fixture.getProject("Snake");
    RenderingService service = RenderServiceFactory.createService(snakePrj, 
        createTestConfiguration(testedPlatform), testedPlatform);
    Reader layoutReader = new InputStreamReader(someLayout1.getInputStream());
    String themeName = "";
    boolean isProjectTheme = false;
    
    Image img = renderImage(service, ap.getLookup().lookup(AndroidClassPath.class),
        layoutReader, themeName, isProjectTheme, "snake_layout");
    assertNotNull(img);
  }
}
