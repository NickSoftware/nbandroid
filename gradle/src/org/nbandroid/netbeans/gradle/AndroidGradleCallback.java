package org.nbandroid.netbeans.gradle;

import org.netbeans.api.project.Project;

/**
 *
 * @author radim
 */
public interface AndroidGradleCallback {

  void customize(AndroidGradleExtension ext, Project prj);
}
