package org.nbandroid.netbeans.ext;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.nbandroid.netbeans.ext.navigation.ProjectResourceLocator;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.LookupProvider;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 * Action provider of the Android project.
 */
@LookupProvider.Registration(
    projectTypes=@LookupProvider.Registration.ProjectType(id="org-netbeans-modules-android-project", position=410))
public class NBAndroidExtLookupProvider implements LookupProvider {

  private static final Logger LOG = Logger.getLogger(NBAndroidExtLookupProvider.class.getName());

  @Override
  public Lookup createAdditionalLookup(Lookup lkp) {
    LOG.log(Level.FINER, "createAdditionalLookup {0}", lkp);
    final Project aPrj = lkp.lookup(Project.class);
    return Lookups.singleton(new ProjectResourceLocator(aPrj));
  }
}
