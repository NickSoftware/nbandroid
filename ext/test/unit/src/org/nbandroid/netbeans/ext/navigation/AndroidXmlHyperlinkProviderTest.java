package org.nbandroid.netbeans.ext.navigation;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import javax.lang.model.element.Element;
import javax.swing.text.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openide.filesystems.FileObject;

import org.netbeans.api.java.source.TestUtilities;
import org.netbeans.api.lexer.Language;
import org.netbeans.api.xml.lexer.XMLTokenId;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.FileUtilities;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.openide.cookies.EditorCookie;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;

import static org.netbeans.modules.android.project.AndroidTestFixture.SDK_DIR;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.api.AndroidFileTypes;

public class AndroidXmlHyperlinkProviderTest {
  
  private File tempFolder;
  private FileObject scratch;
  private FileObject sdkDirFo;
  private FileObject source;

  @Before
  public void setUp() throws Exception {
    org.netbeans.api.project.ui.OpenProjects.getDefault().getOpenProjects();
    
    MockServices.setServices();
    tempFolder = File.createTempFile("junit", "");
    tempFolder.delete();
    tempFolder.mkdir();

    scratch = FileUtil.toFileObject(tempFolder);
    sdkDirFo = FileUtil.toFileObject(new File(SDK_DIR));
  }

  @After
  public void tearDown() {
    FileUtilities.recursiveDelete(tempFolder);
  }
  
  @Test
  public void testSkeletonActivity() throws Exception {
    List<ResourceRef> links = Lists.newArrayList(
      new ResourceRef(true, null, "string", "back", "Back"),
      new ResourceRef(true, null, "string", "clear", "Clear"),
      new ResourceRef(true, null, "color", "red", null),
      new ResourceRef(true, null, "drawable", "semi_black", null),
      new ResourceRef(true, null, "drawable", "violet", null)
    );
    List<ElementRef> elems = Lists.newArrayList(
        new ElementRef("LinearLayout", "android.widget.LinearLayout"),
        new ElementRef("EditText", "android.widget.EditText"),
        new ElementRef("Button", "android.widget.Button"),
        new ElementRef("ImageView", "android.widget.ImageView")
    );
    doTestXml("samples/android-15/SkeletonApp/res/layout/skeleton_activity.xml", 
        links, elems);
  }
  
  
  @Test
  public void testXmlTagInManifest() throws Exception {
    List<ResourceRef> links = Lists.newArrayList(
      new ResourceRef(true, null, "drawable", "app_icon", null),
      new ResourceRef(true, null, "string", "app_name", null),
      new ResourceRef(true, null, "string", "app_descrip", null),
      new ResourceRef(true, null, "string", "widget_name", null),
      new ResourceRef(true, null, "xml", "searchable", null),
      new ResourceRef(true, null, "xml", "widget_word", null)
    );
    List<ElementRef> elems = Lists.newArrayList(
        new ElementRef(".LookupActivity", "com.example.android.wiktionary.LookupActivity"),
        new ElementRef(".WordWidget", "com.example.android.wiktionary.WordWidget"),
        new ElementRef(".WordWidget$UpdateService", "com.example.android.wiktionary.WordWidget.UpdateService")
    );
    doTestXml("samples/android-17/Wiktionary/AndroidManifest.xml", 
        links, elems);
  }
  
  private static class ElementRef {
    public final String refText;
    public final String elementName;

    public ElementRef(String refText, String elementName) {
      this.refText = refText;
      this.elementName = elementName;
    }
  }

  /** Binding between editor document to resolve classpath and find classes there for test. 
   * Replaces search doc->fileobject->project->classpath and static SourceUtils.getFile().
   */
  private class ClasspathResolverStub implements XmlGoToSupport.DependencyResolver {

    // cannot mock so create some cp.
    private final ClasspathInfo cpInfo = ClasspathInfo.create(
        sdkDirFo.getFileObject("samples/android-17/Wiktionary/src/com/example/android/wiktionary/LookupActivity.java"));
    private final FileObject fo = mock(FileObject.class);
    private final String clzName;

    public ClasspathResolverStub(String clzName) {
      this.clzName = clzName;
    }
    
    @Override
    public ClasspathInfo cpInfoForXmlDocument(Document doc) {
      return cpInfo;
    }

    @Override
    public FileObject findFile(ElementHandle<? extends Element> handle, ClasspathInfo cpInfo) {
      return handle.getQualifiedName().equals(clzName) ? fo : null;
    }

    @Override
    public DalvikPlatform platform(Document doc) {
      return null;
    }
  }
  
  private void doTestXml(String resourceName, List<ResourceRef> links, List<ElementRef> elems) throws Exception {
    InputStream sourceIS = 
        sdkDirFo.getFileObject(resourceName).getInputStream();
    final String sourceTxt = CharStreams.toString(new InputStreamReader(sourceIS));
    Document doc = prepareTest(sourceTxt);
    for (int i = 0; i < doc.getLength(); i++) {
      final int current = i;
      UiUtilsCaller callback = mock(UiUtilsCaller.class);
      AndroidFileTypes fileTypes = mock(AndroidFileTypes.class);
      when(fileTypes.isLayoutFile(any(FileObject.class))).thenReturn(resourceName.contains("/res/layout"));
      XmlGoToSupport goToSupport;
      ResourceRef id = Iterables.find(links, new Predicate<ResourceRef>() {
            @Override
            public boolean apply(ResourceRef t) {
              String refString = t.toRefString();
              int idx = sourceTxt.indexOf(refString);
              return current >= idx && current < idx + refString.length();
            }
          },
          null);
      boolean expectXmlHyperlink = id != null;
      if (expectXmlHyperlink) {
        when(callback.open(
            eq(source), 
            eq(new ResourceRef(true, null /*"com.example.android.skeletonapp"*/, id.resourceType, id.resourceName, null)))).
        thenReturn(true);
      }
      
      ElementRef elemId = Iterables.find(elems, new Predicate<ElementRef>() {
            @Override
            public boolean apply(ElementRef t) {
              int idx = sourceTxt.indexOf("android:name=\"" + t.refText + "\"");
              if (idx >= 0 &&
                  current >= idx + "android:name=\"".length() && 
                  current < idx + t.refText.length()  + "android:name=\"".length()) {
                return true;
              }
              idx = sourceTxt.indexOf("<" + t.refText + " ");
              if (idx >= 0 &&
                  current >= idx + "<".length() && 
                  current < idx + t.refText.length()  + "<".length()) {
                return true;
              }
              return false;
            }
          },
          null);
      boolean expectJavaHyperlink = elemId != null;
      if (expectJavaHyperlink) {
        when(callback.open(
            any(ClasspathInfo.class), 
            any(ElementHandle.class))).
        thenReturn(true);
        goToSupport = new XmlGoToSupport(new ClasspathResolverStub(elemId.elementName), fileTypes, callback);
      } else {
        goToSupport = new XmlGoToSupport(callback);
      }
      
      performTest(doc, i, goToSupport, expectXmlHyperlink || expectJavaHyperlink);
      if (expectXmlHyperlink) {
        verify(callback).open(
            eq(source), 
            eq(new ResourceRef(true, null /*"com.example.android.skeletonapp"*/, id.resourceType, id.resourceName, null)));
      } else if (expectJavaHyperlink) {
        verify(callback).open(
            any(ClasspathInfo.class), 
            any(ElementHandle.class));
      } else {
        verifyZeroInteractions(callback);
      }
    }
  }

  private Document prepareTest(String sourceCode) throws Exception {
    FileUtil.refreshFor(tempFolder);

    FileObject sourceDir = FileUtil.createFolder(scratch, "src");

    source = FileUtil.createData(sourceDir, "test/test.xml");

    TestUtilities.copyStringToFile(source, sourceCode);

    DataObject od = DataObject.find(source);
    EditorCookie ec = od.getLookup().lookup(EditorCookie.class);
    Document doc = ec.openDocument();

    doc.putProperty(Language.class, XMLTokenId.language());
    doc.putProperty("mimeType", "text/xml");
    return doc;
  }

  private void performTest(
      Document doc, final int offset, final XmlGoToSupport goToSupport, boolean isHyperlink) throws Exception {
    String sample = doc.getText(offset, Math.min(25, doc.getLength() - offset));
    if (isHyperlink) {
      assertNotNull("hyperlink at offset " + offset + ": " + sample, goToSupport.getIdentifierSpan(doc, offset));
      goToSupport.goTo(doc, offset);
    } else {
      assertNull("no hyperlink at offset " + offset + ": " + sample, goToSupport.getIdentifierSpan(doc, offset));
    }
  }
}
