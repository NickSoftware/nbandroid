package org.nbandroid.netbeans.ext.navigation;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;
import org.openide.filesystems.FileObject;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.netbeans.modules.android.project.api.ResourceRef;

/**
 *
 * @author radim
 */
public class UiUtilsCallerImplTest {
  
  private static AndroidTestFixture fixture;
  private static FileObject skeletonPrjDir;
  private static AndroidProject skeletonPrj;
  private static FileObject snakePrjDir;
  private static AndroidProject snakePrj;
  private static FileObject someSource1;
  private static FileObject skeletonLayout;
  private static FileObject someLayout1;
  
  private static FileObject wiktionaryPrjDir;
  private static AndroidProject wiktionaryPrj;
  private static DalvikPlatform testedPlatform;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Snake", "samples/android-8/Snake")
        .withProject("SkeletonApp", "samples/android-8/SkeletonApp")
        .withProject("Wiktionary", "samples/android-17/Wiktionary");

    snakePrjDir = fixture.getProjectFolder("Snake");
    snakePrj = (AndroidProject) fixture.getProject("Snake");
    someSource1 = snakePrjDir.getFileObject("src/com/example/android/snake/Snake.java");
    someLayout1 = snakePrjDir.getFileObject("res/layout/snake_layout.xml");

    skeletonPrjDir = fixture.getProjectFolder("SkeletonApp");
    skeletonPrj = (AndroidProject) fixture.getProject("SkeletonApp");
    skeletonLayout = skeletonPrjDir.getFileObject("res/layout/skeleton_activity.xml");

    wiktionaryPrjDir = fixture.getProjectFolder("Wiktionary");
    wiktionaryPrj = (AndroidProject) fixture.getProject("Wiktionary");
    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget("android-8");
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }
  
  @Test
  public void xmlResource() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    FileObject wiktionaryXml = wiktionaryPrjDir.getFileObject("res/xml/searchable.xml");
    when(open.doOpen(wiktionaryXml)).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertTrue(ui.open(wiktionaryPrjDir.getFileObject("AndroidManifest.xml"), new ResourceRef(true, null, "xml", "searchable", null)));
    verify(open).doOpen(wiktionaryXml);
  }
  
  @Test
  public void drawableName() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    FileObject redstar = snakePrjDir.getFileObject("res/drawable/redstar.png");
    when(open.doOpen(redstar)).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertTrue(ui.open(someSource1, new ResourceRef(true, "com.example.android.snake", "drawable", "redstar", null)));
    verify(open).doOpen(redstar);
  }
  
  @Test
  public void layoutName() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    when(open.doOpen(someLayout1)).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    // TODO package is wrong here
    assertTrue(ui.open(someSource1, new ResourceRef(true, "com.example.android.skeletonapp", "layout", "snake_layout", null)));
    verify(open).doOpen(someLayout1);
  }
  
  @Test
  public void idInLayout() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    // TODO 32 is better (tag spans 32-37 and id is on 32)
    when(open.doOpenLine(any(FileObject.class), anyInt())).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "id", 
        "editor", null)));
    verify(open).doOpenLine(skeletonLayout, 36);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "id", 
        "back", null)));
    verify(open).doOpenLine(skeletonLayout, 55);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "id", 
        "clear", null)));
    verify(open).doOpenLine(skeletonLayout, 66);
  }
  
  @Test
  public void strings() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    FileObject strings = skeletonPrjDir.getFileObject("res/values/strings.xml");
    // TODO 32 is better (tag spans 32-37 and id is on 32)
    when(open.doOpenLine(any(FileObject.class), anyInt())).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "string", 
        "main_label", null)));
    verify(open).doOpenLine(strings, 26);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "string", 
        "back", null)));
    verify(open).doOpenLine(strings, 22);
    assertTrue(ui.open(
        skeletonPrjDir.getFileObject("src/com/example/android/skeletonapp/SkeletonActivity.java"), 
        new ResourceRef(true, "com.example.android.skeletonapp", 
        "string", 
        "clear", null)));
    verify(open).doOpenLine(strings, 23);
  }
  
  @Test
  public void styleables() {
    UiUtilsCallerImpl.OpenExecutor open = mock(UiUtilsCallerImpl.OpenExecutor.class);
    FileObject attrs = snakePrjDir.getFileObject("res/values/attrs.xml");
    when(open.doOpenLine(any(FileObject.class), anyInt())).thenReturn(Boolean.TRUE);
    UiUtilsCallerImpl ui = new UiUtilsCallerImpl(open);
    assertTrue(ui.open(
        snakePrjDir.getFileObject("src/com/example/android/snake/TileView.java"), 
        new ResourceRef(true, "com.example.android.snake", 
        "styleable", 
        "TileView", null)));
    verify(open).doOpenLine(attrs, 17);
    assertTrue(ui.open(
        snakePrjDir.getFileObject("src/com/example/android/snake/TileView.java"), 
        new ResourceRef(true, "com.example.android.snake", 
        "styleable", 
        "TileView_tileSize", null)));
    verify(open).doOpenLine(attrs, 18);
  }
}